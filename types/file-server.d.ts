export = index;
declare class index {
  constructor(errorCallback: any, cacheSize: any);
  cache: any;
  errorCallback: any;
  getFile(stats: any, fileName: any, mimeType: any, maxAge: any, request: any, response: any): any;
  serveDirectory(rootDirectory: any, mimeTypes: any, maxAge: any): any;
  serveFile(fileName: any, mimeType: any, maxAge: any): any;
}
