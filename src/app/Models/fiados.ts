import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("Fiados")
export class Fiados {
  @JsonProperty('ID_FIADO', Number)
  ID_FIADO: number = undefined;
  @JsonProperty('ID_GRUPO', Number)
  ID_GRUPO: number = undefined;
    @JsonProperty('RFC', String)
    RFC: String = undefined;
    @JsonProperty('NOMBRE', String)
    NOMBRE: String = undefined;
    @JsonProperty('ACTIVO', Boolean)
    ACTIVO: Boolean = undefined;
  }