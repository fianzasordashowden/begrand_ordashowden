
import {JsonProperty, JsonObject, JsonConverter, JsonCustomConvert} from "json2typescript";
import { FileComentDTO } from "./FileComentDTO";

@JsonObject("PostRegisterComentNuevo")
export class PostRegisterComentNuevo {
    @JsonProperty('userId', Number)
    userId:  number = undefined;
    @JsonProperty('numSol', Number)
    numSol:  Number = undefined;
    @JsonProperty('idDoc', Number)
    idDoc:  Number = undefined;
    @JsonProperty('desComent', String)
    desComent:  String = undefined;
    @JsonProperty('fileComentDTO',  [FileComentDTO])
    fileComentDTO: FileComentDTO [] ;
    @JsonProperty('ID_CONTROL_GRUPO', Number)
    ID_CONTROL_GRUPO:  Number = undefined;
  }