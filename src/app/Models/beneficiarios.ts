import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("Beneficiarios")
export class Beneficiarios {
    @JsonProperty('CVE_BENEF', Number)
    CVE_BENEF: number = undefined;
    @JsonProperty('CVE_CORPORATIVO', Number)
    CVE_CORPORATIVO: number = undefined;
    @JsonProperty('CVE_GRUPO', Number)
    CVE_GRUPO: number = undefined;
    @JsonProperty('RFC_BENEF', String)
    RFC_BENEF: String = undefined;
    @JsonProperty('RAZON_SOCIAL_BENEF', String)
    RAZON_SOCIAL_BENEF: String = undefined;
    @JsonProperty('ACTIVO', Boolean)
    ACTIVO: Boolean = undefined;
  }