
import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("PostTokenNotificacion")
export class PostTokenNotificacion {
    @JsonProperty('ID_USUARIO', Number)
    ID_USUARIO:  string = undefined;
    @JsonProperty('TOKEN', String)
    TOKEN:  string = undefined;    
  }