import { InfoUsuario } from "./infoUser";
import { SchemaUsuario } from "./shemaUsu";
import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("Usuario")
export class Usuario {
    @JsonProperty('access_token', String)
    accToken: string = undefined
    @JsonProperty('token_type', String)
    tokenType: String = undefined
    @JsonProperty('expires_in', Number)
    exp: number = undefined
    @JsonProperty('as:client_id', String)
    clieId:  string = undefined
    @JsonProperty('infoUser', [InfoUsuario])
    info: InfoUsuario = undefined
    @JsonProperty('schemaUser', [SchemaUsuario])
    shema: SchemaUsuario[] = undefined
    @JsonProperty('userName', String)
    name: string = undefined
    @JsonProperty('.issued', String)
    dtIn: string = undefined
    @JsonProperty('.expires', String)
    dtOff: string = undefined
  
}
  