import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("FuncUsuario")
export class FuncUsuario {
    @JsonProperty('idFuncion',Number)
    funId: number = undefined;
    @JsonProperty('idRol', Number)
    rolId: number = undefined;
    @JsonProperty('funcion', String)
    descFun: string = undefined;
    @JsonProperty('url', String)
    url:  string = undefined;
    @JsonProperty('icono', String )
    descIcon: string = undefined;
 
  }
