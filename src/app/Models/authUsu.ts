import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("AuthDataCli")
export class authCli {
    @JsonProperty('credCliPlat', String)
    CredentialPlatform: string = undefined;
    @JsonProperty('sigCli', String)
    Signature: String = undefined;
    @JsonProperty('credCli', String)
    CredentialData:  String = undefined;
    @JsonProperty('credPassCli', String)
    CredentialPwd:  String = undefined;
    @JsonProperty('tiempCli', String)
    Timestamp:  String = undefined;
    @JsonProperty('credLoginCli', String)
    CredentialLogin:  String = undefined;
    @JsonProperty('idLoginCli', Number)
    idLoginCli:  number = undefined;
  }