
import {JsonProperty, JsonObject, JsonConverter, JsonCustomConvert} from "json2typescript";

@JsonObject("FileComentDTO")
export class FileComentDTO {
    @JsonProperty('nameFile', String)
    nameFile:  String = undefined;
    @JsonProperty('numDoc', Number)
    numDoc:  number = 0;
    @JsonProperty('sizeFile', String)
    sizeFile:  String = undefined;
    @JsonProperty('extFile', String)
    extFile:  String = undefined;
    @JsonProperty('typeFile', String)
    typeFile:  String = undefined;
    @JsonProperty('idTipFile', Number)
    idTipFile:  number = undefined;
    @JsonProperty('imgFile', String)
    imgFile:  String = undefined;
    @JsonProperty('dataFile', String)
    dataFile:  String = undefined;
  
  }