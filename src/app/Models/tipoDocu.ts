import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("TipoDocumento")
export class TipoDocumento {
    @JsonProperty('CVE_TIPO_DOC', Number)
    CVE_TIPO_DOC: number = undefined;
    @JsonProperty('DES_DOCUMENTO', String)
    DES_DOCUMENTO: String = undefined;
    @JsonProperty('idTipDoc', String)
    idTipDoc: String = "";
  }





