
import {JsonProperty, JsonObject, JsonConverter, JsonCustomConvert} from "json2typescript";

@JsonObject("FilesSolNuevaDTO")
export class FilesSolNuevaDTO {
    @JsonProperty('nameFile', String)
    nameFile:  String = undefined;
    @JsonProperty('numDoc', Number)
    numDoc:  number = 0;
    @JsonProperty('sizeFile', String)
    sizeFile:  String = undefined;
    @JsonProperty('extFile', String)
    extFile:  String = undefined;
    @JsonProperty('typeFile', String)
    typeFile:  String = undefined;
    @JsonProperty('idTipFile', Number)
    idTipFile:  number = undefined;
    @JsonProperty('imgFile', String)
    imgFile:  String = undefined;
    @JsonProperty('dataFile', String)
    dataFile:  String = undefined;
  
  }


@JsonObject("fileFianza")
export class fileFianza {
    @JsonProperty('CONTEXTO', String)
    CONTEXTO:  String = undefined;
    @JsonProperty('DES_TIP', String)
    DES_TIP:  String  = undefined;
    @JsonProperty('FS_DOCUMENTO', String)
    FS_DOCUMENTO:  String = undefined;
    @JsonProperty('ID_DOCUMENTO', Number)
    ID_DOCUMENTO:  number = undefined;
    @JsonProperty('ID_TIPO', Number)
    ID_TIPO:  number = undefined;
    @JsonProperty('NOM_DOCUMENTO', String)
    NOM_DOCUMENTO:  String = undefined;
    @JsonProperty('NUM_SOL', Number)
    NUM_SOL:  number = undefined;
    @JsonProperty('b_DOCUMENTO', String)
    b_DOCUMENTO:  String = undefined;
    @JsonProperty('s_DOCUMENTO', String)
    s_DOCUMENTO:  String = undefined;
    @JsonProperty('DISPLAY_BTN', Boolean)
    DISPLAY_BTN:  boolean = false;
  }