
import {JsonProperty, JsonObject, JsonConverter, JsonCustomConvert} from "json2typescript";

@JsonObject("PostChangePass")
export class PostChangePass {
    @JsonProperty('ID_USUARIO', Number)
    ID_USUARIO:  number = undefined;
    @JsonProperty('OLD_PASS', String)
    OLD_PASS:  string = undefined;
    @JsonProperty('NEW_PASS', String)
    NEW_PASS:  string = undefined;
    
  }