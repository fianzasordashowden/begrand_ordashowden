import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("ClientToken")
export class Client {
    @JsonProperty('cliTokPlat', String)
    ClientTokenPlatform: string = undefined;
    @JsonProperty('cliTok', String)
    ClientToken: String = undefined;
    @JsonProperty('cliId', Number)
    ClientId:  number = undefined;
  }