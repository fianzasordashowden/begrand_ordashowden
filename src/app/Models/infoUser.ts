import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("InfoUsuario")
export class InfoUsuario {
    @JsonProperty('nombreCompleto', String)
    nombre: string = undefined;
    @JsonProperty('fechaAlta', String)
    fecha: String = undefined;
    @JsonProperty('celular', String)
    phone:  number = undefined;
    @JsonProperty('idRol', String)
    rolId:  number = undefined;
    @JsonProperty('Rol', String)
    rol:  string = undefined;
    @JsonProperty('foto', String)
    urlFoto:  string = undefined;
    @JsonProperty('idUsuario', String)
    idUsu: number = undefined;

  }



