import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("TipoMovimiento")
export class TipoMovimiento {
    @JsonProperty('CVE_TIPO_DOC', Number)
    cveTipDoc: number = undefined;
    @JsonProperty('DES_DOCUMENTO', String)
    desTipDoc: String = undefined;
    @JsonProperty('ACTIVO', String)
    actDoc: String = undefined;
  }


