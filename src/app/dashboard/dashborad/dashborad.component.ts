import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
declare var $;

import { DeviceDetectorService } from 'ngx-device-detector';


@Component({
  selector: 'app-dashborad',
  templateUrl: './dashborad.component.html',
  styleUrls: ['./dashborad.component.scss']
})
export class DashboradComponent implements OnInit, OnDestroy {

  @ViewChild('dataTable') table;
  dataTable: any;

  //Device info
  deviceInfo = null;

  constructor(
    private deviceService: DeviceDetectorService
  ) { 
    
  }

  
  ngOnInit() {

    $(document).ready(() => {
      
      const trees: any = $('[data-widget="tree"]');
      trees.tree();

      this.dataTable = $(this.table.nativeElement);
      this.dataTable.dataTable({
        responsive: true
      });

    });


    
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.deviceInfo = this.deviceService.getDeviceInfo();
    console.log(this.deviceInfo);


  }

    ngOnDestroy(): void {
        document.body.className = '';
    }

}
