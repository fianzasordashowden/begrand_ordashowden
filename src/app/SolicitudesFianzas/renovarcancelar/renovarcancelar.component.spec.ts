import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenovarcancelarComponent } from './renovarcancelar.component';

describe('RenovarcancelarComponent', () => {
  let component: RenovarcancelarComponent;
  let fixture: ComponentFixture<RenovarcancelarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenovarcancelarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenovarcancelarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
