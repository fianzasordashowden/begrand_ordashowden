import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { trigger, state, style, animate, transition} from '@angular/animations';
import { SatCalendar, SatDatepicker, DateAdapter,  MAT_DATE_LOCALE, MAT_DATE_FORMATS, NativeDateAdapter} from 'saturn-datepicker';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import { rootGlobals } from 'src/app/rootGlobals';
import { SrvsolicitudesService } from '../services/srvsolicitudes.service';
import { SecurityService } from 'src/app/Servicios/security.service';
import { ReporteCancelarRenovar } from 'src/app/Models/fileNewSol';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'L',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-renovarcancelar',
  templateUrl: './renovarcancelar.component.html',
  styleUrls: ['./renovarcancelar.component.scss'],
  animations: [
    trigger('changeDivSize', [
      state('initial', style({
        backgroundColor: 'green',
        width: '100px',
        height: '100px'
      })),
      state('final', style({
        backgroundColor: 'red',
        width: '200px',
        height: '200px'
      })),
      transition('initial=>final', animate('1500ms')),
      transition('final=>initial', animate('1000ms'))
    ]),

    trigger('balloonEffect', [
      state('initial', style({
        backgroundColor: 'green',
        transform: 'scale(1)'
      })),
      state('final', style({
        backgroundColor: 'red',
        transform: 'scale(1.5)'
      })),
      transition('final=>initial', animate('1000ms')),
      transition('initial=>final', animate('1500ms'))
    ]),

    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),

    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ]),
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
            animate(300, style({ opacity: 0 }))
      ])
    ])
  ],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
  ],
})

export class RenovarcancelarComponent implements OnInit 
{

  altSolForm: FormGroup;
  dateNow: any;
  reporteCancelarRenovar: ReporteCancelarRenovar = new ReporteCancelarRenovar();
  nameTit: string = "";

  constructor(private formBuilder: FormBuilder,
    public renderer: Renderer2,
    public globals: rootGlobals,
    private solicitudService: SrvsolicitudesService,
    private securityServices: SecurityService,
    private spinner: NgxSpinnerService,) 
  {
    moment.locale('es');

    setInterval(() => {
      this.dateNow = moment().format('LLLL');
    }, 1);
  }

  ngOnInit() 
  {
    this.altSolForm = this.formBuilder.group({
      fecha_inicio:    [ {value: '', disabled: false},  Validators.compose([Validators.required])],
      fecha_fin:    [ {value: '', disabled: false},  Validators.compose([Validators.required])],
      slRango:             ['', Validators.compose([Validators.required] )],
    });
  }

  ReporteRenovarCancelar()
  {
    debugger;
    this.spinner.show();

    this.reporteCancelarRenovar.fecha_inicio = this.altSolForm.get('fecha_inicio').value;
    this.reporteCancelarRenovar.fecha_final = this.altSolForm.get('fecha_fin').value;
    this.reporteCancelarRenovar.margen = Number(this.altSolForm.get('slRango').value);
    this.reporteCancelarRenovar.id_aplicacion = Number(this.globals.aplicacionID);

    this.solicitudService.ReporteProximasCancelarRenovar(this.reporteCancelarRenovar).subscribe(response => 
    {
      let archivo = response.FileStream._buffer;
      this.solicitudService.downloadEXCEL(archivo, response.FileDownloadName);
      this.spinner.hide();
    },error => 
    {
      this.spinner.hide();
      this.securityServices.clearSession();
    });

  }

}
