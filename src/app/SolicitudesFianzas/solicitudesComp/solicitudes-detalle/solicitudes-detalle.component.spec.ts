import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudesDetalleComponent } from './solicitudes-detalle.component';

describe('SolicitudesDetalleComponent', () => {
  let component: SolicitudesDetalleComponent;
  let fixture: ComponentFixture<SolicitudesDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitudesDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolicitudesDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
