import { ToastOptions } from "ng6-toastr/src/toast-options";


export class CustomOption extends ToastOptions {
    //toastLife = 50000;
    animate = 'flyRight'; // you can pass any options to override defaults
    newestOnTop = true;
    showCloseButton = true;
    dismiss = 'auto';
    positionClass = 'toast-bottom-left';
}