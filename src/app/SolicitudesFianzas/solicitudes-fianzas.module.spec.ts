import { SolicitudesFianzasModule } from './solicitudes-fianzas.module';

describe('SolicitudesFianzasModule', () => {
  let solicitudesFianzasModule: SolicitudesFianzasModule;

  beforeEach(() => {
    solicitudesFianzasModule = new SolicitudesFianzasModule();
  });

  it('should create an instance', () => {
    expect(solicitudesFianzasModule).toBeTruthy();
  });
});
