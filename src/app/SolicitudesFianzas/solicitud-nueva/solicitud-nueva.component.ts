import { Component, OnInit, Renderer2, ElementRef, ViewChild, Input, Output, EventEmitter, HostListener, Inject } from '@angular/core';
import { trigger, state, style, animate, transition} from '@angular/animations';
import { HttpClient} from '@angular/common/http';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn } from '@angular/forms';

import { NgxSpinnerService } from 'ngx-spinner';

import { Router } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';

import * as moment from 'moment';
import {Any, JsonConvert, JsonConverter, OperationMode, ValueCheckingMode} from "json2typescript"
import Swal from 'sweetalert2'

//MODELS
import { solicitudNuevaDTONew } from 'src/app/Models/solicitudes';
import { TipoSolicitudNew } from 'src/app/Models/tipoSolicitud';
import { TipoDocumento } from 'src/app/Models/tipoDocu';
import { FilesSolNuevaDTO } from 'src/app/Models/filesSolNuevaDTO';
import { PostRegisterSolNueva } from 'src/app/Models/PostRegisterSolNueva';


//SERVICES
import { SrvsolicitudesService } from '../services/srvsolicitudes.service';
import { TipoSubRamo } from 'src/app/Models/fiaCore';
import { SecurityService } from 'src/app/Servicios/security.service';
import { validateStyleParams } from '@angular/animations/browser/src/util';
import { distinctUntilChanged } from 'rxjs/operators';
import { Console } from '@angular/core/src/console';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Fiados } from 'src/app/Models/fiados';
import { Beneficiarios } from 'src/app/Models/beneficiarios';

declare var $;


export interface DialogDataFiado {
  rfcFiado: string;
  rfcFiadoConf: string;
  nameFiado: string;
  boolFiado: boolean;
  lstFiados: Array<Fiados>;
}

export interface DialogDataBenef {
  rfcBenef: string;
  rfcBenefConf: string;
  nameBenef: string;
  boolBenef: boolean;
  lstBenef: Array<Beneficiarios>;
}

export interface ResultData {
  rfcFiado: String;
  nameFiado: String;
  blNewFiado: boolean;
  rfcBenef: String;
  nameBenef: String;
  blNewBenef: boolean;
}


@Component({
  selector: 'app-solicitud-nueva',
  templateUrl: './solicitud-nueva.component.html',
  styleUrls: ['./solicitud-nueva.component.scss'],
  animations: [
    trigger('changeDivSize', [
      state('initial', style({
        backgroundColor: 'green',
        width: '100px',
        height: '100px'
      })),
      state('final', style({
        backgroundColor: 'red',
        width: '200px',
        height: '200px'
      })),
      transition('initial=>final', animate('1500ms')),
      transition('final=>initial', animate('1000ms'))
    ]),

    trigger('balloonEffect', [
      state('initial', style({
        backgroundColor: 'green',
        transform: 'scale(1)'
      })),
      state('final', style({
        backgroundColor: 'red',
        transform: 'scale(1.5)'
      })),
      transition('final=>initial', animate('1000ms')),
      transition('initial=>final', animate('1500ms'))
    ]),

    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),

    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ]),
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
            animate(300, style({ opacity: 0 }))
      ])
    ])
  ]
})
export class SolicitudNuevaComponent implements OnInit {

  @ViewChild("hDocs") hDocs: ElementRef;

  nameTit: string = "Nueva";
  altSolForm: FormGroup;
  solNew: solicitudNuevaDTONew = new solicitudNuevaDTONew();
  usuIdSol: any;
  dateNow: any;
  divDocs; any;
  arrTipSols: TipoSolicitudNew []; 
  arrSubRamos: TipoSubRamo []; 
  arrTipFia: TipoSolicitudNew[];
  idAfi: number;
  idRamo: number;
  idSubRamo: number;
  bolOk: Boolean = false;
  arrTipDocs: Array<TipoDocumento> = [];
  arrFileDocs: Array<FilesSolNuevaDTO> = [];
  postRegNewSol: PostRegisterSolNueva = new PostRegisterSolNueva();
  fileUpload: HTMLInputElement;
  value: number = 50;
  divDatFiado: Boolean = false;
  fiadoOther: Boolean = false;

  fiadoNew: DialogDataFiado = {
    rfcFiado: '',
    rfcFiadoConf: '',
    nameFiado: '',
    boolFiado: false,
    lstFiados: []
  }

  benefNew: DialogDataBenef = {
    rfcBenef: '',
    rfcBenefConf: '',
    nameBenef: '',
    boolBenef: false,
    lstBenef: []
  }
  
  fiaBenfResult: ResultData;


//FileUpload
/*
//***************************************************************************************************************** /**         Link text */
      @Input() text = 'Upload';
      /** Name used in form which will be sent in HTTP request. */
      @Input() param = 'file';
      /** Target URL for file uploading. */
      @Input() target = 'https://file.io';
      /** File extension that accepted, same as 'accept' of <input type="file" />. 
          By the default, it's set to 'image/*'. */
      @Input() accept = 'image/*, .pdf, .doc, .xls, .xlsx, .docx, ppt, pptx, .xml';
      /** Allow you to add handler after its completion. Bubble up response text from remote.**/
      @Output() complete = new EventEmitter<string>();

     // private filesUpMod: Array<FileUploadModel> = [];

      formData = new FormData(); 

      stMatPro: string = "void";
   
      private filesLoad: any[] = [];
      private fileReader = new FileReader();


//******************************************************************************************************************/
  constructor(
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private solicitudService: SrvsolicitudesService,
    private securityServices: SecurityService,
    public renderer: Renderer2,
    private _http: HttpClient,
    private router: Router,
    public session: SessionStorageService,
    public dialog: MatDialog,
    
  ) { 
    
    this.usuIdSol = JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value);

    moment.locale('es');


    setInterval(() => {
      this.dateNow = moment().format('LLLL');
    }, 1);
    
    this.fiaBenfResult= {
      rfcFiado: "",
      nameFiado: "",
      blNewFiado: false,
      rfcBenef: "",
      nameBenef: "",
      blNewBenef: false,
    };


  }

  ngOnInit() {
    //Validacion de los campos del formulario
    this.altSolForm = this.formBuilder.group({
      slRamo:             ['', Validators.compose([Validators.required] )],
      slSubRamo:          ['', Validators.compose([Validators.required] )],
      slTipFia:           ['', Validators.compose([Validators.required] )],
      slBenf:             ['', Validators.compose([Validators.required] )],
      slTipMov:           ['', Validators.compose([Validators.required] )],
      slAfin:             ['', Validators.compose([Validators.required] )],
      slProyecSol:        ['', Validators.compose([Validators.required] )],
      slFiad:             ['', Validators.compose([Validators.required] )],
      observaciones:      ['', Validators.compose([Validators.required] )],
      inRfcFiado:         ['', Validators.compose([])],
      inRfcFiadoConf:     ['', Validators.compose([])],
      nombreFiado:        ['', Validators.compose([])],  
      inRfcBenef:         ['', Validators.compose([])],
      inRfcBenefConf:     ['', Validators.compose([])],
      nombreBenef:        ['', Validators.compose([])],  
    }
    // ,{ validators: [this.fiadoBEnefMatchValidator()] }
    );

  
    /***********************************************************************************************************/

    this.setUserFiadoValidators();
    //this.getSolicitudNueva();

    /***********************************************************************************************************/

    document.querySelector("html").classList.add('js');

  

  }

  ngOnDestroy(): void {
    document.body.className = '';
  }

  ngAfterViewInit() {
    // viewChild is set after the view has been initialized
    console.log('AfterViewInit');

this.spinner.show();  

    this.getSolicitudNueva();
              
  }


  //------------------------------ Metodos ----------------------------------------------
  getSolicitudNueva() {

      

    if(
      this.solNew.lstTipRamos.length == 0 ||
      this.solNew.lstTipSubRamos.length == 0 ||
      this.solNew.lstTipMovimiento.length == 0 ||
      this.solNew.lstTipSol.length == 0 ||
      this.solNew.lstAfianzadoras.length == 0 ||
      this.solNew.lstBeneficiarios.length == 0 ||
      this.solNew.lstFiados.length == 0)
    {
      this.solicitudService.getNuevaSolicitud().subscribe(
        data => {
          
          this.spinner.show();  

          //Filtramos por grupo
          //let id_grupo : Number = this.usuIdSol.id_grupo;
          let id_grupo : Number = 6;

          var arrRamos    =   JSON.stringify(data.solicitudNuevaDTO.lstTipRamos);
          var arrSubRamos =   JSON.stringify(data.solicitudNuevaDTO.lstTipSubRamos);
          var arrTipSOl   =   JSON.stringify(data.solicitudNuevaDTO.lstTipSolicitudes);
          var arrTipMov   =   JSON.stringify(data.solicitudNuevaDTO.lstTipMovimientos);
          var arrAfianz   =   JSON.stringify(data.solicitudNuevaDTO.lstAfianzadoras);
          var arrBenef    =   JSON.stringify(data.solicitudNuevaDTO.lstBeneficiarios);
          var arrFiad     =   JSON.stringify(data.solicitudNuevaDTO.lstFiados);
          var arrProy     =   JSON.stringify(data.solicitudNuevaDTO.lstProyectos);
  
          this.solNew.lstTipRamos       = JSON.parse(arrRamos);
          this.solNew.lstTipSubRamos    = JSON.parse(arrSubRamos);
          this.solNew.lstTipMovimiento  = JSON.parse(arrTipMov);
          this.solNew.lstTipSol         = JSON.parse(arrTipSOl);
          this.solNew.lstProyectos      = JSON.parse(arrProy).sort();
          this.solNew.lstAfianzadoras   = JSON.parse(arrAfianz).filter(x => x.ID_GRUPO == id_grupo);
          
          this.solNew.lstBeneficiarios  = JSON.parse(arrBenef);
          this.benefNew.lstBenef = JSON.parse(arrBenef);


          //Usuario proveedor tiene un fiado fijo
          if( this.usuIdSol.CvePerfil != 8 ){
            this.fiadoOther = true;
            this.solNew.lstFiados = JSON.parse(arrFiad).filter(x => x.ID_GRUPO == id_grupo);
            this.fiadoNew.lstFiados =  JSON.parse(arrFiad).filter(x => x.ID_GRUPO == id_grupo);
          }else{
            this.fiadoOther = false;
            this.solNew.lstFiados = JSON.parse(arrFiad).filter(x => x.ID_GRUPO == id_grupo && x.ID_FIADO == this.usuIdSol.idFiado);
            this.fiadoNew.lstFiados =  JSON.parse(arrFiad).filter(x => x.ID_GRUPO == id_grupo && x.ID_FIADO == this.usuIdSol.idFiado);
          }

          this.spinner.hide();

        },
        // Si ducede algun error se limpia la sessión y redirige al login
        error => {
          this.spinner.hide();
          this.securityServices.clearSession();
          console.log('oops: ', error)
        },
        () => {        

          // if(id != ''){
          //   var evt = document.createEvent("MouseEvents");
          //   evt.initMouseEvent("click", true, true, window,
          //     0, 0, 0, 0, 0, false, false, false, false, 0, null);
          //   var a = document.getElementById(id.toString()); 
          //   a.dispatchEvent(evt); 
          // }
          
          this.spinner.hide();
        }    
      );
    }
    else{
      this.spinner.hide();
    }
  }

 
//Ok Form Alta Solicitud
//*************************************************************************************************************/

  onAltaSol(){

    if (this.altSolForm.valid ) {

      Swal.fire({
        title: 'Generar una Solictud',
        text: "¿Estas seguro de generar una solictud?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI, estoy de acuerdo!'
      }).then((result) => {
    
        this.bolOk =  (result.value === 'true') ? true : false; 
        
        this.spinner.show();  

        if(result.value){

          if ( this.arrFileDocs.length > 0  ) {
    
            this.postRegNewSol.userName = this.session.get("nameUsuFia");
            this.postRegNewSol.userId =   this.usuIdSol.idUsuario;
            this.postRegNewSol.userDate = moment().format();
            this.postRegNewSol.clieId = 1;  
            this.postRegNewSol.gereId = 1; 
            
            this.postRegNewSol.ramoId     = this.altSolForm.get('slRamo').value;
            this.postRegNewSol.subRamoId  = this.altSolForm.get('slSubRamo').value;
            this.postRegNewSol.paqId      = this.altSolForm.get('slTipFia').value;
            this.postRegNewSol.tipMoviId  = this.altSolForm.get('slTipMov').value;
            this.postRegNewSol.afianId    = this.altSolForm.get('slAfin').value;

            //Fiado es "OTRO" IPR
            if (this.altSolForm.get('slFiad').value == 0){               
              this.postRegNewSol.fiadoRFC     = this.fiaBenfResult.rfcFiado;
              this.postRegNewSol.fiadoRFCConf = this.fiaBenfResult.rfcFiado;
              this.postRegNewSol.fiadoNombre  = this.fiaBenfResult.nameFiado;
              this.postRegNewSol.fiadoGrupo   = this.usuIdSol.id_grupo;
            }else{              
              this.postRegNewSol.fiadoId    = this.altSolForm.get('slFiad').value;
              this.postRegNewSol.fiadoRFC    = this.solNew.lstFiados.filter(x => x.ID_FIADO == this.altSolForm.get('slFiad').value)[0].RFC.toString();
              this.postRegNewSol.fiadoNombre = this.solNew.lstFiados.filter(x => x.ID_FIADO == this.altSolForm.get('slFiad').value)[0].NOMBRE.toString();
              this.postRegNewSol.fiadoGrupo = this.usuIdSol.id_grupo;
            }

             //Beneficiario es "OTRO" IPR
             if (this.altSolForm.get('slBenf').value == 0){               
              this.postRegNewSol.benefRFC     = this.fiaBenfResult.rfcBenef;
              this.postRegNewSol.benefRFCConf = this.fiaBenfResult.rfcBenef;
              this.postRegNewSol.benefNombre  = this.fiaBenfResult.nameBenef
              this.postRegNewSol.benefGrupo   = this.usuIdSol.id_grupo;
            }else{   
              this.postRegNewSol.benfId     = this.altSolForm.get('slBenf').value; 
              this.postRegNewSol.benefRFC = this.solNew.lstBeneficiarios.filter(x => x.CVE_BENEF == this.altSolForm.get('slBenf').value)[0].RFC_BENEF.toString();
              this.postRegNewSol.benefNombre = this.solNew.lstBeneficiarios.filter(x => x.CVE_BENEF == this.altSolForm.get('slBenf').value)[0].RAZON_SOCIAL_BENEF.toString();
              this.postRegNewSol.benefGrupo = this.usuIdSol.id_grupo;
            }

            this.postRegNewSol.tipProy =  this.altSolForm.get('slProyecSol').value;
            this.postRegNewSol.detSol =   this.altSolForm.get('observaciones').value;

            // this.postRegNewSol.observaciones = this.altSolForm.get('observaciones').value;
            this.postRegNewSol.filesSolNuevaDTO = this.arrFileDocs;

            this.postRegNewSol.id_control_grupo = this.usuIdSol.id_grupo; //agrego 09/02/21 ahernandezh

            this.solicitudService.postNuevaSolicitud(this.postRegNewSol).subscribe(
                  data => {

                    if(data.codeResult == 0){
  
                      this.spinner.hide();  
                      
                      Swal.fire(
                        'Gurdado!',
                        '¡Solicitud: ' + data.solicitudInsDTO.NUM_SOL + ' gurdada con exito!',
                        'success'
                      )
                      
                      this.router.navigate(["/solicitudes", 3]).then( (e) => {
                      });
  
                    }else{
                      this.spinner.hide();  
                      Swal.fire(
                        'Error!',
                        '¡Contactar a Sistemas Ordás-Howden!',
                        'error'
                      )
                    }
                  },
                  // Si ducede algun error se limpia la sessión y redirige al login
                  error => {
                    this.spinner.hide();
                    this.securityServices.clearSession();
                    console.log('oops: ', error)
                  }
                );
  
          }else{
            this.spinner.hide();  
            Swal.fire(
              'Error!',
              '¡Debes cargar al menos 1 documento!',
              'error'
            )            
          }        
        }else{
          this.spinner.hide();  
            Swal.fire(
              'Error!',
              '¡Decidiste cancelar la operación!',
              'error'
            )
        }
      });   
    }
  }

//FileUploas Methods
//*************************************************************************************************************/
  onClick(id) {
      this.stMatPro = "in";
      var tipDoc =  this.arrTipDocs.filter(
                    doc => doc.CVE_TIPO_DOC === id);
        this.fileUpload = document.getElementById(id) as HTMLInputElement;
        this.fileUpload.click();
        this.stMatPro = "void";
  }

  onChange(event: Event, id) {
    let evFiles = event.target['files'];

    var tipDoc =  this.arrTipDocs.filter(doc => doc.CVE_TIPO_DOC === id);
    this.fileUpload = document.getElementById(id) as HTMLInputElement;
    document.getElementById(tipDoc[0].idTipDoc.toString()).innerHTML = evFiles[0].name;

    if (event.target['files']) {
      this.readFiles(event.target['files'], 0, id);
    }
  };

  private readFiles(filesLoad: FileDataForm, index: number, idDoc: number) {
    
    let file = filesLoad[index];
    let fileNewSol: FilesSolNuevaDTO = new FilesSolNuevaDTO();
   
    fileNewSol.nameFile = file.name.split('.')[0] + "." + file.name.split('.').pop();
    fileNewSol.sizeFile = this.formatBytes(file.size, 0).toString();
    fileNewSol.extFile = file.name.split('.').pop();
    fileNewSol.idTipFile = idDoc;
    fileNewSol.typeFile = file.type.toString();

    this.fileReader.onload = () => {
      fileNewSol.dataFile = this.fileReader.result.toString();
      this.arrFileDocs = this.arrFileDocs.filter(function (p){ return p.idTipFile != idDoc});
      this.arrFileDocs.push(fileNewSol);
      this.formData.append(fileNewSol.nameFile.toString(), file);
    };
    this.fileReader.readAsDataURL(file);
  }

  formatBytes(bytes, decimals) {
    if (bytes == 0) return 0;
    var k = 1024;
    var dm = decimals + 1 || 2;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  // changeFiado (ev) {
  //   if(ev.value == 0){
  //     this.fiaBenfResult.blNewFiado = false;
  //     // this.divDatFiado = true;
  //     // this.altSolForm.controls['inRfcFiado'].setValidators([Validators.compose([Validators.required])]);
  //     // this.altSolForm.controls['inRfcFiadoConf'].setValidators([Validators.compose([Validators.required])]);
  //     // this.altSolForm.controls['nombreFiado'].setValidators([Validators.compose([Validators.required])]);
  //   }
  //   else{
  //     this.fiaBenfResult.blNewFiado = true;
  //     // this.divDatFiado = false;
  //     // this.altSolForm.controls['inRfcFiado'].clearValidators();
  //     // this.altSolForm.controls['inRfcFiadoConf'].clearValidators();
  //     // this.altSolForm.controls['nombreFiado'].clearValidators();
  //   }
  // }

  // changeBenef (ev) {
  //   if(ev.value == 0){
  //     this.fiaBenfResult.blNewBenef = false;
     
  //   }
  //   else{
  //     this.fiaBenfResult.blNewBenef = true;
     
  //   }
  // }

  //Coments
  /************************************************************************************************************/
  getAfianzadora (ev) {
    this.idAfi = ev.source.value;
  }

  getSubRamo(ev) {
      
    this.arrSubRamos = this.solNew.lstTipSubRamos;
    this.idRamo = ev.source.value;
    this.arrSubRamos =  this.arrSubRamos.filter( tipSubRamo => tipSubRamo.CVE_RAMO === this.idRamo);
      
  }

  getTipFianza(ev) {
    
    this.arrTipFia = this.solNew.lstTipSol;
    this.idSubRamo = ev.source.value;
    //Añadir CVE de la afianzadora
    //Cambio IPR 17/12/20
    //this.arrTipFia =  this.arrTipFia.filter( tipFia => tipFia.CVE_RAMO === this.idRamo &&  tipFia.CVE_SUBRAMO === this.idSubRamo && tipFia.CVE_AFIANZADORA == this.idAfi);
    this.arrTipFia.sort((a,b) => a.DES_PAQUETE.localeCompare(b.DES_PAQUETE.toString()));

  }

  showFilesFia(ev){
    
      this.arrTipSols = this.solNew.lstTipSol;
      let idTipPaq = ev.source.value;
      //Cambio IPR 17/12/20
      //var selectedIds =  this.arrTipSols.filter(tipSol => tipSol.CVE_PAQUETE === idTipPaq && tipSol.CVE_RAMO === this.idRamo && tipSol.CVE_SUBRAMO === this.idSubRamo);
      var selectedIds =  this.arrTipSols.filter(tipSol => tipSol.CVE_PAQUETE === idTipPaq && tipSol.CVE_RAMO === this.idRamo );

      this.arrTipDocs = selectedIds[0].lstTipDocumentos;
      
      var prueba = this.arrTipDocs[0].DES_DOCUMENTO.toString().replace(/\s/g, ""); 

      this.arrTipDocs.forEach( obj => {
        obj.idTipDoc = obj.DES_DOCUMENTO.toString().replace(/\s/g, ""); 
      
      })    
  }

  // changeRfc(){

  //   if(this.altSolForm.get('inRfcFiado').value != ''){
      
  //     this.altSolForm.controls['inRfcFiadoConf'].enable();  

  //   }

  //   this.solNew.lstFiados.forEach( (fia) => {
  //     if(this.altSolForm.get('inRfcFiado').value == fia.RFC){
  //       Swal.fire(
  //         'Error!',
  //         '¡El RFC del fiado ya existe en su grupo, favor de seleccionarlo en la lista!',
  //         'error'
  //       );
  //       this.altSolForm.controls['inRfcFiado'].setValue('');
  //       this.altSolForm.controls['inRfcFiadoConf'].setValue('');
  //     }  
  //   });
  // }

  // changeRfcConf(){

  //   if(this.altSolForm.get('inRfcFiado').value != this.altSolForm.get('inRfcFiadoConf').value){
      
  //     Swal.fire(
  //       'Error!',
  //       '¡Los RFC no coinciden, intente de nuevo!',
  //       'error'
  //     );
  //     this.altSolForm.controls['inRfcFiadoConf'].setValue('');
  //   }
  // }

  setUserFiadoValidators() {
    

    //FIADO IPR
    const getFiado = this.altSolForm.get('slFiad');
    const rfcFiadoControl = this.altSolForm.get('inRfcFiado');
    const rfcFiadoConfControl = this.altSolForm.get('inRfcFiadoConf');
    const nomFiadoControl = this.altSolForm.get('nombreFiado');

    this.altSolForm.get('slFiad')
    .valueChanges.pipe(distinctUntilChanged())
    .subscribe(userFiado => {

      if (userFiado === 0) {
        // this.divDatFiado = true;
        rfcFiadoControl.setValidators([Validators.required]);
        rfcFiadoConfControl.setValidators([Validators.required]);
        nomFiadoControl.setValidators(Validators.required);
        getFiado.setValidators(null);

      }else{
        // this.divDatFiado = false;
        rfcFiadoControl.setValidators(null);
        rfcFiadoConfControl.setValidators(null);
        nomFiadoControl.setValidators(null);
        getFiado.setValidators(Validators.required);
      }

      getFiado.updateValueAndValidity();
      rfcFiadoControl.updateValueAndValidity();
      rfcFiadoConfControl.updateValueAndValidity();
      nomFiadoControl.updateValueAndValidity();
    });


     //BENEFICIARIO IPR
     const getBenef = this.altSolForm.get('slBenf');
     const rfcBenefControl = this.altSolForm.get('inRfcBenef');
     const rfcBenefConfControl = this.altSolForm.get('inRfcBenefConf');
     const nomBenefControl = this.altSolForm.get('nombreBenef');
 
     this.altSolForm.get('slBenf')
     .valueChanges.pipe(distinctUntilChanged())
     .subscribe(userBenef => {
 
       if (userBenef === 0) {
         // this.divDatFiado = true;
         rfcBenefControl.setValidators([Validators.required]);
         rfcBenefConfControl.setValidators([Validators.required]);
         nomBenefControl.setValidators(Validators.required);
         getBenef.setValidators(null);
 
       }else{
         // this.divDatFiado = false;
         rfcBenefControl.setValidators(null);
         rfcBenefConfControl.setValidators(null);
         nomBenefControl.setValidators(null);
         getBenef.setValidators(Validators.required);
       }
 
       getBenef.updateValueAndValidity();
       rfcBenefControl.updateValueAndValidity();
       rfcBenefConfControl.updateValueAndValidity();
       nomBenefControl.updateValueAndValidity();
     });



  }


  openDialogFiado(): void {
    const dialogRef = this.dialog.open(DialogNewFiado, {
        width: '500px',
        data: {
            rfcFiado: this.fiadoNew.rfcFiado.toString(),
            rfcFiadoConf: this.fiadoNew.rfcFiadoConf.toString(),
            nameFiado: this.fiadoNew.nameFiado.toString(),
            boolFiado: this.fiadoNew.boolFiado.toString(),  
            arrFiados: this.fiadoNew.lstFiados
        }
    });

    dialogRef.afterClosed().subscribe(result => {
        if (result !== undefined) {

          this.fiaBenfResult.nameFiado = result.nameFiado;
          this.fiaBenfResult.rfcFiado = result.rfcFiado;
          this.fiaBenfResult.blNewFiado = true;

          this.altSolForm.controls['inRfcFiado'].setValue(result.rfcFiado);
          this.altSolForm.controls['inRfcFiadoConf'].setValue(result.rfcFiado);
          this.altSolForm.controls['nombreFiado'].setValue(result.nameFiado);

        }
    });
  }


  openDialogBenef(): void {
    const dialogRef = this.dialog.open(DialogNewBenef, {
        width: '500px',
        data: {
            rfcBenef: this.benefNew.rfcBenef.toString(),
            rfcBenefConf: this.benefNew.rfcBenefConf.toString(),
            nameBenef: this.benefNew.nameBenef.toString(),
            boolBenef: this.benefNew.boolBenef.toString(),  
            arrBenef: this.benefNew.lstBenef
        }
    });

    dialogRef.afterClosed().subscribe(result => {
        if (result !== undefined) {

          this.fiaBenfResult.nameBenef = result.nameBenef;
          this.fiaBenfResult.rfcBenef = result.rfcBenef;
          this.fiaBenfResult.blNewBenef = true;

          this.altSolForm.controls['inRfcBenef'].setValue(result.rfcBenef);
          this.altSolForm.controls['inRfcBenefConf'].setValue(result.rfcBenef);
          this.altSolForm.controls['nombreBenef'].setValue(result.nameBenef);

        }
    });
  }


}



@Component({
  selector: 'dialogNewFiado',
  templateUrl: 'newFiado.html',
})
export class DialogNewFiado {

  public dataResult: ResultData;
  newFiadoForm: FormGroup;
  dataFiado: any;
  rfcValidados: boolean = false;
  

  constructor(
      public dialogFiado: MatDialogRef<DialogNewFiado>,
      private formBuilder: FormBuilder,
      @Inject(MAT_DIALOG_DATA) public data: DialogDataFiado) {
      this.dataFiado = data;
      this.dataResult = {
        rfcFiado: "",
        nameFiado: "",
        blNewFiado: false,
        rfcBenef: "",
        nameBenef: "",
        blNewBenef: false,
      };
  }

  ngOnInit() {
    this.newFiadoForm = this.formBuilder.group({
      rfcFiado:     ['', Validators.compose([Validators.required] )],
      rfcFiadoConf: ['', Validators.compose([Validators.required] )],
      nameFiado:    ['', Validators.compose([Validators.required] )],
    },
    { validator: rfcMatchValidator },
    );

    function rfcMatchValidator(frm: FormGroup) {
        return frm.controls['rfcFiado'].value === frm.controls['rfcFiadoConf'].value ? null : {'mismatch': true};
    }

}

  valuechange(event, data): void {
      this.dataFiado = data;
      // this.Comisiones();
  }


  changeRfc(){

    if(this.dataFiado.arrFiados.length > 0 ){

      this.dataFiado.arrFiados.forEach( (fia) => {
        if(this.newFiadoForm.get('rfcFiado').value == fia.RFC){
          Swal.fire(
            'Error!',
            '¡El RFC del fiado ya existe en su grupo, favor de seleccionarlo en la lista!',
            'error'
          );
        }  
      });

    }
    
  }

  changeRfcConf(){

    let rfc1 = this.newFiadoForm.get('rfcFiado').value;
    let rfc2 =  this.newFiadoForm.get('rfcFiadoConf').value;

    if( rfc1 !== rfc2 ){
      
      Swal.fire(
        'Error!',
        '¡Los RFC no coinciden, intente de nuevo!',
        'error'
      );
      
    }
  }

  onClick() {

    let fiaFound: boolean = false

    if(!this.newFiadoForm.invalid){

      this.dataFiado.arrFiados.forEach( (fia) => {
        if(this.newFiadoForm.get('rfcFiado').value == fia.RFC){
          Swal.fire(
            'Error!',
            '¡El RFC del fiado ya existe en su grupo, favor de seleccionarlo en la lista!',
            'error'
          );

          fiaFound = true;
        }
      });

      if(!fiaFound){
        this.dataResult.rfcFiado = String(this.newFiadoForm.get('rfcFiado').value).toUpperCase();
        this.dataResult.nameFiado = String(this.newFiadoForm.get('nameFiado').value).toUpperCase();
        this.dataResult.blNewFiado = true;
      }



    }

    this.dialogFiado.close(this.dataResult);
  }

  onNoClick(): void {
    this.dialogFiado.close();
  }

  
}



@Component({
  selector: 'dialogNewBenef',
  templateUrl: 'newBenef.html',
})
export class DialogNewBenef {

  public dataResult: ResultData;
  newBenForm: FormGroup;
  dataBenef: any;
  rfcValidados: boolean = false;
  
  constructor(
      public dialogBenef: MatDialogRef<DialogNewBenef>,
      private formBuilder: FormBuilder,
      @Inject(MAT_DIALOG_DATA) public data: DialogDataBenef) {
      this.dataBenef = data;
      this.dataResult = {
        rfcFiado: "",
        nameFiado: "",
        blNewFiado: false,
        rfcBenef: "",
        nameBenef: "",
        blNewBenef: false,
      };
  }

  ngOnInit() {
    this.newBenForm = this.formBuilder.group({
      rfcBenef:     ['', Validators.compose([Validators.required] )],
      rfcBenefConf: ['', Validators.compose([Validators.required] )],
      nameBenef:    ['', Validators.compose([Validators.required] )],
    },
    { validator: rfcMatchValidator },
    );

    function rfcMatchValidator(frm: FormGroup) {
        return frm.controls['rfcBenef'].value === frm.controls['rfcBenefConf'].value ? null : {'mismatch': true};
    }
}

  valuechange(event, data): void {
      this.dataBenef = data;
      // this.Comisiones();
  }


  changeRfc(){

    if(this.dataBenef.arrBenef.length > 0 ){

      this.dataBenef.arrBenef.forEach( (fia) => {
        if(this.newBenForm.get('rfcBenef').value == fia.RFC){
          Swal.fire(
            'Error!',
            '¡El RFC del fiado ya existe en su grupo, favor de seleccionarlo en la lista!',
            'error'
          );
        }  
      });

    }
    
  }

  changeRfcConf(){

    let rfc1 = this.newBenForm.get('rfcBenef').value;
    let rfc2 =  this.newBenForm.get('rfcBenefConf').value;

    if( rfc1 !== rfc2 ){
      
      Swal.fire(
        'Error!',
        '¡Los RFC no coinciden, intente de nuevo!',
        'error'
      );
      
    }
  }

  onClick() {

    let benFound: boolean = false

    this.dataBenef.arrBenef.forEach( (fia) => {
      if(this.newBenForm.get('rfcBenef').value == fia.RFC_BENEF){
        Swal.fire(
          'Error!',
          '¡El RFC del beneficiario ya existe en su grupo, favor de seleccionarlo en la lista!',
          'error'
        );

        benFound = true;
      }
            
    });

    if(!benFound){
      this.dataResult.rfcBenef = String(this.newBenForm.get('rfcBenef').value).toUpperCase();
      this.dataResult.nameBenef = String(this.newBenForm.get('nameBenef').value).toUpperCase();
      this.dataResult.blNewBenef = true;
    }

  
    this.dialogBenef.close(this.dataResult);

  }

  onNoClick(): void {
    this.dialogBenef.close();
  }


  
}