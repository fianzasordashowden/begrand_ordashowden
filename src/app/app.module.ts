import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//Modules Propios
import { LoginModule} from './login/login.module';
import { DashboardModule} from './dashboard/dashboard.module';
import { SolicitudesFianzasModule} from './SolicitudesFianzas/solicitudes-fianzas.module';
import { ConsultaFianzasModule } from './Fianzas/consulta-fianzas.module';

//ngx-device-detector
import { DeviceDetectorModule } from 'ngx-device-detector';
//angular-webstorage-service
import { AngularWebStorageModule } from 'angular-web-storage';
//NgxSpinnerModule
import { NgxSpinnerModule } from 'ngx-spinner';

import { HttpConfigInterceptor } from './Servicios/httpconfig.interceptor';
import { ConsultaFianzasComponent } from './Fianzas/consulta-fianzas/consulta-fianzas.component';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireModule } from '@angular/fire';
import { AsyncPipe } from '@angular/common';
import { MessagingService } from './shared/messaging.service';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
      
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    LoginModule,
    DashboardModule,
    SolicitudesFianzasModule,
    DeviceDetectorModule.forRoot(),
    AngularWebStorageModule,
    NgxSpinnerModule,
    ConsultaFianzasModule,
    
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase)   

  ],
  providers: [
    { 
      provide: HTTP_INTERCEPTORS, 
      useClass: HttpConfigInterceptor, 
      multi: true 
    },
    MessagingService, 
    AsyncPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
