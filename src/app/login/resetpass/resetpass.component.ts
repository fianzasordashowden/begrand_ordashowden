import { FormBuilder, FormGroup, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { SessionStorageService, SessionStorage, StorageService } from 'angular-web-storage';
import { NgxSpinnerService} from 'ngx-spinner'
import { SecurityService } from 'src/app/Servicios/security.service';
import { PostChangePass } from 'src/app/Models/PostChangePass';

function equalsValidator(otherControl: AbstractControl): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    const value: any = control.value;
    const otherValue: any = otherControl.value;
    return otherValue === value ? null : { 'notEquals': { value, otherValue } };
  };
}

export const CustomValidators = {
  equals: equalsValidator
};

@Component({
  selector: 'app-resetpass',
  templateUrl: './resetpass.component.html',
  styleUrls: ['./resetpass.component.scss']
})

export class ResetpassComponent implements OnInit {

  resetPassForm: FormGroup;
  recuperacionForm: FormGroup;
  
  postChangePass: PostChangePass = new PostChangePass();

  public isError = false;
  noFoundUsu:boolean = false;
  
  params = new URLSearchParams(location.search);


  constructor(
      private formBuilder: FormBuilder,
      private securityServices: SecurityService,
      public session: SessionStorageService,
      private spinner: NgxSpinnerService,
  ) { 

    this.resetPassForm = this.formBuilder.group({
      inNewPass: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      inNewPassC: ''
    });

    this.resetPassForm.get('inNewPassC').setValidators(
      CustomValidators.equals(this.resetPassForm.get('inNewPass'))
    );
  }

  ngOnInit() {

  }

  onResetPass(){
    
    if (this.resetPassForm.valid) {

      if(parseInt(this.params.get('userId')) > 0){
        this.postChangePass.ID_USUARIO = parseInt(this.params.get('userId'));
        this.postChangePass.NEW_PASS = this.resetPassForm.get('inNewPass').value as string;
        this.postChangePass.OLD_PASS = 'r3s3tp@s5w0rd';
      }
      else{
        this.securityServices.validateSession();          
      }

      this.spinner.show();

      //FGonzalez: Validamos la session llamando la función "loginuser" que se encuentra en nuestro securityServices
      return this.securityServices
        .changePass(this.postChangePass)
        //Nos suscribimos al servicio esperando un data con los parametros que enviamos a la función
        .subscribe(
          data => {
            if (data) {  
              
              var objAll = JSON.parse(JSON.stringify(data));
              var mensaje = objAll.solicitudChangePassDTO.MENSAJE;

              document.getElementById('resultMsg').innerHTML = mensaje  +  ". Por favor da clic <a href='/login'>aquí</a> para iniciar sesión con tu nueva contraseña";

              this.resetPassForm.reset();

              this.spinner.hide();

            }
            // Si no trea la data correctamente o trae otra cosa se limpia la sesión y redirige al login
            else{
              this.securityServices.clearSession();
              this.noFoundUsu = true;
            } 
          },
          // Si ducede algun error se limpia la sessión y redirige al login
          error => {
            this.spinner.hide();
            this.securityServices.clearSession();
            this.noFoundUsu = true;
          }
        );
    } else {
      this.onIsError();
      //this.router.navigate(['login']);
      this.noFoundUsu = true;
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 500);
    }
  }

  onIsError(): void {
    this.isError = true;
    setTimeout(() => {
      this.isError = false;
      this.spinner.hide();
    }, 500);
  }

}
