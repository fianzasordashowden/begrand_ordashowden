import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import { SecurityService } from 'src/app/Servicios/security.service';

@Component({
  selector: 'app-asidenavbar',
  templateUrl: './asidenavbar.component.html',
  styleUrls: ['./asidenavbar.component.scss']
})
export class AsidenavbarComponent implements OnInit {

  // FGonzalez: Declaramos nuestro arreglo de funciones qasideue de momento lo dejamos como null  
  funciones = null;
  
  constructor(
    private router: Router,
    private securityServices: SecurityService,
  ) { 
    // FGonzalez: Lo primero que se hace es validar si la sessión esta activa.
    // En caso de que no la misma función validateSession() nos redireccionara al login
    if (this.securityServices.validateSession()){
      //Con la session activa llenamos nuestro arreglo de funciones que es utilizado en el html
      this.funciones = JSON.parse(JSON.parse(sessionStorage.getItem('schemaUser'))._value).Funciones;
    }
  }
  
  ngOnInit() {   
  }

  accSol( url, id) {

    switch(id) { 
      case 6 :
      case 8 : { 
        this.router.navigate([url]).then( (e) => {
          if (e) {
          } else {
          }
        });
         break; 
      } 
      default: { 

        

        if(id) {
          this.router.navigate([url, id]).then( (e) => {
            if (e) {
            } else {
            }
          });
    
        }else{
          this.router.navigate([url]).then( (e) => {
            if (e) {
            } else {
            }
          });
        }
         break; 
      } 
   }  

 
  }
}
