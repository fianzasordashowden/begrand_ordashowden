import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FianzasDetalleComponent } from './fianzas-detalle.component';

describe('FianzasDetalleComponent', () => {
  let component: FianzasDetalleComponent;
  let fixture: ComponentFixture<FianzasDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FianzasDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FianzasDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
