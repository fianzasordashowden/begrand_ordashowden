import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Forms
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DataViewModule } from 'primeng/dataview';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

//Material Desing
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//DATAPICKER RANGE
import { MyDateRangePickerModule } from 'mydaterangepicker';

import { MatTableModule, MatFormFieldModule, MatInputModule, MatPaginatorModule,
         MatButtonModule, MatIconModule, MatDialogModule, MatTabsModule,
         MatCardModule, MatSlideToggleModule, MatSelectModule, MatProgressBarModule, MatDatepickerModule,
         MatNativeDateModule } from '@angular/material';

import { NgxSpinnerModule} from 'ngx-spinner'

import { NgxCurrencyModule } from "ngx-currency";

 //Carga de Layout 
 import { LayoutModule } from 'src/app/layout/layout.module';
 import { AppRoutingModule } from '../app-routing.module';

 import { ConsultaFianzasComponent } from './consulta-fianzas/consulta-fianzas.component';
 import { FianzasDetalleComponent } from './fianzas-detalle/fianzas-detalle.component';

//IPR Root Variables
import { rootGlobals } from '../rootGlobals';

//Servicios Solicitudes
import { FianzasService } from '../Fianzas/services/fianzas.service';
import { RouterModule } from '@angular/router';
import { FianzasVencComponent } from './fianzas-venc/fianzas-venc.component';



@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    //Shared View
    LayoutModule,
    //DateRange OK
    MyDateRangePickerModule,
    DataViewModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatTabsModule,
    MatCardModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    NgxCurrencyModule,

  ],
  declarations: [
    ConsultaFianzasComponent,
    FianzasDetalleComponent,
    FianzasVencComponent
  ],
  entryComponents: [
    FianzasDetalleComponent
  ],
  //IPR Services
  providers: [
    FianzasService,
   //Root Variables
   rootGlobals]
})
export class ConsultaFianzasModule { }
