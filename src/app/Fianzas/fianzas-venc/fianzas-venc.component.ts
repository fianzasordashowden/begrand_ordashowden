import { Component, OnInit, ViewChild, Input, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import { ActivatedRoute } from '@angular/router';

//Tools
import { ConfirmationService } from 'primeng/primeng';
import { NgxSpinnerService } from 'ngx-spinner';
import * as XLSX from 'xlsx';
import Scene from "scenejs";
import * as moment from 'moment';

//Servicios
import { FianzasService } from 'src/app/Fianzas/services/fianzas.service';
import { SecurityService } from 'src/app/Servicios/security.service';

//Models
import { solicitudFianzasDTO, solicitudAccionaFianzasDTO, solExcelGO, solicitudAccionaFianzasLst, solicitudDetalleFiaDTO, solExcelAcc, fiaExcelAcc, fiaVenExcelAcc } from 'src/app/Models/solicitudes';

//Components
import { fileFianza, porcentajeFiaDTO } from 'src/app/Models/fileNewSol';
import { DomSanitizer } from '@angular/platform-browser';
import { PostInfoUser } from 'src/app/Models/userLog';

@Component({
  selector: 'app-fianzas-venc',
  templateUrl: './fianzas-venc.component.html',
  styleUrls: ['./fianzas-venc.component.scss']
})
export class FianzasVencComponent implements OnInit {

  nameTit: string = "Fianzas";
  solicitudesLst: solicitudAccionaFianzasLst[];
  porcFiaLst : porcentajeFiaDTO[];
  solDetFianza: solicitudDetalleFiaDTO;
  fileDocsLst: Array<fileFianza> = []; 
  fileFianzaLst: Array<fileFianza> = []; 
  show:boolean = false;
  RptExcel: Array<fiaExcelAcc> = [];
  id_rpt : number = 0;
  infUser: PostInfoUser = new PostInfoUser();
  FEC_CUM_FIA
  displayedColumns: string[] = ['NUM_FIA', 'MONTO_FIA', 'DES_FIADO', 'DES_BENEF', 'FEC_CUMP_CONTR', 'FEC_CUM_FIA'];
  dataSourseFia = new MatTableDataSource();

  @Input() childStatusSol: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;



  constructor(
    private fianzasService: FianzasService,
    private securityServices: SecurityService,
    private confirmationService: ConfirmationService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private sanitizer: DomSanitizer,
  ) { }



  ngOnInit() {
  
    this.dataSourseFia.paginator = this.paginator;
    this.dataSourseFia.sort = this.sort;

    this.getAllFianzasVencidas(this.childStatusSol);
    document.getElementById("push-menu").click();
  
  }




  applyFilter(filterValue: string) {
    this.dataSourseFia.filter = filterValue.trim().toLowerCase();
    if (this.dataSourseFia.paginator) {
      this.dataSourseFia.paginator.firstPage();
    }
  }

  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }


//********************************************************************************/

  //Metodos Solicitudes
  getAllFianzasVencidas(statusSol) {

    this.spinner.show();    

    let JsonUsu =  JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value);
      
    this.infUser.cvePerfil = JsonUsu.CvePerfil;
    this.infUser.nameRol = JsonUsu.Rol;
    this.infUser.idArea = JsonUsu.area;
    this.infUser.ejecutivo = JsonUsu.ejecutivo;
    this.infUser.dateAlta =  moment(JsonUsu.fechaAlta, 'DD-MM-YYYY').toDate();
    this.infUser.idUsuario = JsonUsu.idUsuario;
    this.infUser.idGrupo = JsonUsu.id_grupo;
    this.infUser.idFiado = JsonUsu.idFiado;
    this.infUser.eMail = JsonUsu.mail;
    this.infUser.userName = JsonUsu.nombreCompleto;
    this.infUser.idParam = 0;


    
    this.fianzasService.getAllFianzasVencidas(this.infUser).subscribe(
      data => {
        var prueba = data;
        console.log(prueba);
        this.solicitudesLst = data.fianzasAccDTO.lstFianzasDTO;
        this.porcFiaLst = data.fianzasAccDTO.lstPorcFiaDto;
        this.dataSourseFia.data =  data.fianzasAccDTO.lstFianzasDTO;

        this.spinner.hide();
        
      },
      // Si ducede algun error se limpia la sessión y redirige al login
      error => {
        this.spinner.hide();
        this.securityServices.clearSession();
        console.log('oops: ', error)
      }
    );

  
  } 

  export() {
    
    this.spinner.show();    

    //cve perfil
    let cve_perfil = JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value).CvePerfil;
    let nombre_archivo = "";

    this.RptExcel = [];

    this.solicitudesLst.forEach( (sol) => {
      let element;

      if(cve_perfil == 6){
        element = new fiaVenExcelAcc();
        element.Fianza = sol.NUM_FIA;
        element.Monto_Fia  = sol.MONTO_FIA;
        element.Fiador_Proveedor  =  sol.FIADO_RFC;
        element.Beneficiario  = sol.DES_BENEF;
        element.Fec_Cump_Contrato = (moment(sol.FEC_CUMP_CONTR.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_CUMP_CONTR.toString()).locale('es').format('L').toString() ); 
        element.Fec_Vigencia_Fianza = (moment(sol.FEC_CUM_FIA.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_CUM_FIA.toString()).locale('es').format('L').toString() ); 

      }
      else if (cve_perfil == 7 || cve_perfil == 8){
        element = new fiaVenExcelAcc();
        element.Fianza = sol.NUM_FIA;
        element.Monto_Fia  = sol.MONTO_FIA;
        element.Fiador_Proveedor  =  sol.FIADO_RFC;
        element.Beneficiario  = sol.DES_BENEF;
        element.Fec_Cump_Contrato = (moment(sol.FEC_CUMP_CONTR.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_CUMP_CONTR.toString()).locale('es').format('L').toString() ); 
        element.Fec_Vigencia_Fianza = (moment(sol.FEC_CUM_FIA.toString()).locale('es').format('L').toString() == 'Invalid date' ? 'n/a' : moment(sol.FEC_CUM_FIA.toString()).locale('es').format('L').toString() ); 

      }

      this.RptExcel.push(element);
    
    }); 

    const workBook = XLSX.utils.book_new(); // create a new blank book
    const workSheet = XLSX.utils.json_to_sheet(this.RptExcel);

    XLSX.utils.book_append_sheet(workBook, workSheet, 'Fianzas-Vencimiento'); // add the worksheet to the book
    XLSX.writeFile(workBook, 'Reporte_Fianzas_Ven.xlsx'); // initiate a file download in browser

    this.spinner.hide();
  }


}
