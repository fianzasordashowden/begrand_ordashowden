import { Injectable } from '@angular/core';

//Import variables globales
import { rootGlobals } from 'src/app/rootGlobals';

//IPR Peticiones Rest
import { HttpClient, HttpResponse, HttpHeaders, HttpParams, HttpErrorResponse,} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

import { map, catchError, tap } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {JsonConvert, OperationMode, ValueCheckingMode} from "json2typescript"

//Models
import { solicitudAccionaFianzasDTO } from 'src/app/Models/solicitudes';
import { SecurityService } from 'src/app/Servicios/security.service';

//Prueba IPR
import { NgxSpinnerService } from 'ngx-spinner';
import { PostInfoUser } from 'src/app/Models/userLog';

@Injectable({
  providedIn: 'root'
})
export class FianzasService {

  //FGonzalez: declaramos la variable que usaremos para alamacenar el token
  tokenUsuFia: any;  

  // Asiganamos nuestras URL's de nuestros servicios
  private urlGetFianzas:  string = "SolicitudesBg/GetFianzasBg";
  private urlGetRptFianzas:  string = "Reportes/GetRptFianzasBg";
  private urlGetRptFianzasVen:  string = "SolicitudesBg/GetFianzasVenBg";
  


  constructor(
    private http: HttpClient,
    private varGlob: rootGlobals,
    private securityServices: SecurityService,
    //Pruebas IPR
    private spinner: NgxSpinnerService,
    // Traemos nustras variables globales
    public globals: rootGlobals,
    ) {       
      // FGonzalez: Lo primero que se hace es validar si la sessión esta activa.
      // En caso de que no la misma función validateSession() nos redireccionara al login
      this.securityServices.validateSession();              
    }  


  getAllFianzas(dataForm: PostInfoUser): Observable<any> {

    try{
      // FGonzalez: Armamos el token con la variable de sesion
      this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;

      //Url del servicio
      let urlGetFianzas: string = this.globals.GetUrlServicio() + this.urlGetFianzas;

      let jsonCrt = new JsonConvert();
      jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
      jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
      jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null


      var headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.tokenUsuFia)
      .set('Accept', 'application/json');

      // Armamos el body con los datos del formulario
      const body =  dataForm;

      return this.http
      // .post<solicitudComentarioSolDTO>(
      .post<any>(
        urlGetFianzas, 
         body,    
        {headers: headers}    
      )
      .pipe(map(data => data));

      
    }catch(e) { 
      this.securityServices.clearSession();
    }

  
  }


  ReportFianzas(dataForm: PostInfoUser): Observable<any> {
     
    try {
      // FGonzalez: Armamos el token con la variable de sesion
      this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;
      
      //cve perfil
      let cve_perfil = JSON.parse(JSON.parse(sessionStorage.getItem('infoUser'))._value).CVE_PERFIL;

      //Url del servicio
      //let urlReportPDFSol: string = this.globals.GetUrlServicio() + this.urlGetRptFianzas + "?_num_rpt=" + num_rpt;
      let urlReportPDFSol: string = this.globals.GetUrlServicio() + this.urlGetRptFianzas;

      let jsonCrt = new JsonConvert();
      jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
      jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
      jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

   
      var headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.tokenUsuFia)
      .set('Accept', 'application/json');


      // Armamos el body para este servicio ira vacio
      //const body = new HttpParams().toString();
      const body =  dataForm;

      return this.http
      .post<any>(
        urlReportPDFSol, 
        body,    
        {headers: headers}    
      )
      .pipe(map(data => data));

    }catch(e) { 
      this.securityServices.clearSession();
    } 
  }  


  getAllFianzasVencidas(dataForm: PostInfoUser): Observable<any> {

    try{
      // FGonzalez: Armamos el token con la variable de sesion
      this.tokenUsuFia = JSON.parse(sessionStorage.getItem('TokenUsuFia'))._value;

      //Url del servicio
      let urlGetFianzas: string = this.globals.GetUrlServicio() + this.urlGetRptFianzasVen;

      var headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', this.tokenUsuFia)
      .set('Accept', 'application/json');

      // Armamos el body con los datos del formulario
      const body =  dataForm;

      return this.http
      // .post<solicitudComentarioSolDTO>(
      .post<any>(
        urlGetFianzas, 
         body,    
        {headers: headers}    
      )
      .pipe(map(data => data));

      
    }catch(e) { 
      this.securityServices.clearSession();
    }

  
  }



}
